import game_API
import random
import math

# START CONSTANTS

STANCE_ROCK = "Rock"
STANCE_PAPER = "Paper"
STANCE_SCISSORS = "Scissors"

COUNTERS = {
	STANCE_ROCK: STANCE_PAPER,
	STANCE_PAPER: STANCE_SCISSORS,
	STANCE_SCISSORS: STANCE_ROCK
}

CREEP_FARMERS_MARKET = 0
CREEP_NOOB_ROUTE = 1
CREEP_HYPERBOLIC_TIME_CHAMBER = 2

ROUTES = {
	CREEP_FARMERS_MARKET: [0, 1, 0, 6, 0, 10, 0],
	CREEP_NOOB_ROUTE: [0, 1, 3, 1, 0, 6, 0],
	CREEP_HYPERBOLIC_TIME_CHAMBER: [0, 10, 16, 17, 18, 15, 16, 10, 0]
}

ENABLE_LOGS = False

NO_MORE_UPGRADES_TURN = 300
HEALTH_0_RESPAWN_WAIT_THRESHOLD = 10  # how close health 0 respawn needs to be for us to consider waiting to kill
MAX_MOVE_COUNTER = 7
ROCK_1_RESPAWN_WAIT_THRESHOLD = 7


# END CONSTANTS

class BotError(Exception):
	def __init__(self, msg):
		super().__init__(msg)


class ForsethBot(object):
	def __init__(self, initial_creep=CREEP_NOOB_ROUTE):
		self.opponent_moves = []
		self.player = None
		""":type : game_API.Player"""
		self.opponent = None
		""":type : game_API.Player"""
		self.turn = 0
		self.monster_is_present = False
		self.opponent_is_present = False
		self.monster = None  # this is the monster that occupies the same space as us. will be None if no monster is in our hex.
		self.creep_state = initial_creep
		self.creep_route = ROUTES[initial_creep]
		self.creep_step = 0
		self.health_0 = None
		""":type : game_API.Monster"""
		self.rock_1 = None
		""":type : game_API.Monster"""
		self.paper_3 = None
		""":type : game_API.Monster"""
		self._log = None

		self.route_sequence = [
			CREEP_NOOB_ROUTE,
			CREEP_FARMERS_MARKET,
			CREEP_NOOB_ROUTE,
			CREEP_HYPERBOLIC_TIME_CHAMBER
		]
		self.route_number = 0
		self.is_super_saiyan = False
		self.game = None
		self.was_pointing_at_ground = False

	def log(self, str):
		if ENABLE_LOGS:
			self._log(str)

	def update_game_state(self, game):
		"""
		:type game: game_API.Game
		:param game:
		:rtype: (int, str)
		:return:
		"""
		self._log = game.log
		self.player = game.get_self()
		self.opponent = game.get_opponent()
		self.turn += 1
		self.opponent_is_present = self.opponent.location == self.player.location
		if game.has_monster(self.player.location):
			self.monster_is_present = not game.get_monster(self.player.location).dead
		else:
			self.monster_is_present = False
		if self.monster_is_present:
			self.monster = game.get_monster(self.player.location)
		else:
			self.monster = None
		self.health_0 = [m for m in game.get_all_monsters() if m.location == 0][0]
		self.rock_1 = [m for m in game.get_all_monsters() if m.location == 1][0]
		self.paper_3 = [m for m in game.get_all_monsters() if m.location == 3][0]
		self.game = game

	def get_turn(self):
		self.log("TURN " + str(self.turn) + ": " + str(self.player.__dict__))
		done_fighting, stance = self.get_combat_params()
		destination_hex, waiting_on_mob = self.find_destination_hex()

		if self.about_to_move() and not (destination_hex != self.player.location and self.was_pointing_at_ground):
			# if i will update, and i am NOT pointing at ground, and i WAS pointing at ground, update =\

			# if i will update
			# and NOT (if i WAS POINTING at ground and i'm now NOT), do NOT update
			if not done_fighting or waiting_on_mob:
				self.log("MOVE CANCEL")
				destination_hex = self.player.location
			else:
				self.log("Moving to " + str(destination_hex) + " on this turn...")
				# check if we need to update state
				self.creep_step += 1
				if self.creep_step == (len(self.creep_route) - 1):
					self.creep_state = self.get_next_creep_state()
					self.creep_route = ROUTES[self.creep_state]
					self.creep_step = 0

		if self.player.location != destination_hex and self.about_to_move():
			mon = [m for m in self.game.get_all_monsters() if m.location == destination_hex]
			mon = mon[0] if len(mon) > 0 else None
			if mon is not None:
				stance = COUNTERS[mon.stance]

		self.log("Go to " + str(destination_hex) + " as " + str(stance))

		self.was_pointing_at_ground = self.player.location == destination_hex

		return destination_hex, stance

	def current_creep_node(self):
		# gets the node that the player should be on for cur creep
		return self.creep_route[self.creep_step]

	def next_creep_node(self):
		if self.creep_step + 1 >= len(self.creep_route):
			return None
		else:
			return self.creep_route[self.creep_step + 1]

	def get_combat_params(self):
		"""
		return whether we are going to move at all (don't move if we have work to do still), and return which stance.
		:rtype: (bool, str)
		:return:
		"""

		done_here = False
		stance = random.choice([STANCE_PAPER, STANCE_ROCK, STANCE_SCISSORS])

		# combat is dual-state; we are either fighting a monster or a player (or neither, in which case it doesn't matter)
		do_pvp = False

		if self.opponent_is_present and self.monster_is_present:
			# special set of checks to determine if we should go into pvp or pvm mode when both are present
			if self.turn > NO_MORE_UPGRADES_TURN or self.is_super_saiyan:
				do_pvp = True

		if do_pvp:
			self.log("we are in pvp")
			stance = random.choice([STANCE_PAPER, STANCE_ROCK, STANCE_SCISSORS])
		elif self.monster_is_present:
			self.log("monster is here - " + str(self.monster.__dict__))
			stance = COUNTERS[self.monster.stance]
		else:
			# no one is on this hex; we are done.
			done_here = True

		return done_here, stance

	def about_to_move(self):
		return self.player.movement_counter - self.player.speed <= 1

	def find_destination_hex(self):
		"""
		Return hex to go towards and whether we wait
		:rtype: int, bool
		:return:
		"""
		# sanity check
		if self.player.location != self.current_creep_node():
			raise BotError("Not on proper creep node; route=" + repr(self.creep_route) + ", step=" + repr(self.creep_step) + ", actual=" + str(self.player.location))

		wait_for_mob_kill = False
		# We should generally be arriving perfectly on time for the health-0 spawn.
		# We may be early. If we ARE early, then wait until the dude can spawn and we can kill him before setting next
		# direction
		if self.creep_step == 0 or (self.is_super_saiyan and self.player.location == 0):
			health_0_respawn_time = self.health_0.respawn_counter if self.health_0.dead else 0  # respawn counter not valid from API when health 0 is alive
			# if it's not lower than the respawn thresh, don't even bother
			self.log("HEALTH 0 RESPAWN: " + str(health_0_respawn_time))
			resp_wait = (HEALTH_0_RESPAWN_WAIT_THRESHOLD * (2 if self.is_super_saiyan else 1))
			self.log("SUPER SAIYAN: " + str(self.is_super_saiyan) + ", thresh: " + str(resp_wait))
			self.log("resp_time: " + str(health_0_respawn_time))
			if health_0_respawn_time < resp_wait:
				kill_time = math.ceil(self.health_0.base_health / self.player.paper) + 1
				move_time = MAX_MOVE_COUNTER - self.player.speed
				if health_0_respawn_time + kill_time < move_time:
					# do nothing, continue creep
					pass
				else:
					wait_for_mob_kill = True
		elif self.creep_state == CREEP_NOOB_ROUTE and self.creep_step == 2 and not self.paper_3.dead:  # on our visit of hex 3
			kill_time = math.ceil(self.paper_3.health / self.player.scissors)
			move_time = MAX_MOVE_COUNTER - self.player.speed
			wait_time = kill_time + 1 - move_time
			self.log("WAIT: " + str(wait_time))
			if wait_time > 0:
				wait_for_mob_kill = True

		if wait_for_mob_kill:
			# cancel movement immediately by returning the current location

			self.log("cancel triggered by mob wait")
			return self.player.location, wait_for_mob_kill

		# okay, actually return our next destination
		next_hex = self.next_creep_node(), wait_for_mob_kill

		# sanity check
		if next_hex is None:
			raise BotError("Next hex in creep is none, we should have switched to another already but are stuck at end!")

		return next_hex

	def get_next_creep_state(self):
		"""
		Gets the next creep state that we want to be in after this one.

		:rtype: int
		:return: Returns an int representing the next state to be in
		"""

		if not self.is_super_saiyan:
			self.route_number += 1
		if self.route_number == len(self.route_sequence):
			self.is_super_saiyan = True

		if self.is_super_saiyan:
			return CREEP_FARMERS_MARKET
		else:
			return self.route_sequence[self.route_number]
