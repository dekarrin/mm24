# keep these three import statements
import game_API
import fileinput
import json

# your import statements here
import forsethbot

first_line = True # DO NOT REMOVE

bot = forsethbot.ForsethBot()


# main player script logic
# DO NOT CHANGE BELOW ----------------------------
for line in fileinput.input():
	if first_line:
		game = game_API.Game(json.loads(line))
		first_line = False
		continue
	game.update(json.loads(line))
# DO NOT CHANGE ABOVE ---------------------------

	# code in this block will be executed each turn of the game

	bot.update_game_state(game)
	destination_node, chosen_stance = bot.get_turn()

	# submit your decision for the turn (This function should be called exactly once per turn)
	game.submit_decision(destination_node, chosen_stance)
